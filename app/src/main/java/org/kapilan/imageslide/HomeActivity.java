package org.kapilan.imageslide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.configureViewPager();;
    }

    private void configureViewPager() {

        ViewPager pager = findViewById(R.id.main_viewPager);

        pager.setAdapter(new ImageAdapter(getSupportFragmentManager()));


    }

}
