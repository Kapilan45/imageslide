package org.kapilan.imageslide;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageFragment extends Fragment {

    int[] images = {R.drawable.celio, R.drawable.lacoste, R.drawable.levis, R.drawable.zara,  R.drawable.puma, R.drawable.emporio};

    public ImageFragment() { }

    public static ImageFragment newInstance(int indice) {

        Bundle args = new Bundle();
        args.putInt("val", indice);

        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        ImageView imageView = view.findViewById(R.id.image_viewer);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click in slide teest : " + v.getTag());
               startActivity(new Intent(getContext(), HomeActivity.class));
            }
        });
        imageView.setImageResource(images[getArguments().getInt("val")]);
        imageView.setTag(getArguments().getInt("val"));
        return view;
    }


}
