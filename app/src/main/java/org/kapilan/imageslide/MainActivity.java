package org.kapilan.imageslide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configureSlider();
    }

    private void configureSlider() {

        ViewPager viewPager = findViewById(R.id.main_viewPager2);
        viewPager.setAdapter(new ImageAdapter(getSupportFragmentManager()));

    }
}
